import { Controller } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { UserDto } from './dto/user.dto';
import { GrpcMethod } from '@nestjs/microservices';
import { AUTH_SERVICE_NAME, CreateUserResponse, SignInResponse, ValidateResponse } from './auth.pb';
import { ValidateRequestDto } from './dto/validateRequest.dto';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @GrpcMethod(AUTH_SERVICE_NAME, 'CreateUser')
  createUser(userDto: UserDto): Promise<CreateUserResponse> {
    return this.authService.createUser(userDto);
  }

  @GrpcMethod(AUTH_SERVICE_NAME, 'SignIn')
  signIn(authCredentialsDto: AuthCredentialsDto): Promise<SignInResponse> {
    return this.authService.signIn(authCredentialsDto);
  }

  @GrpcMethod(AUTH_SERVICE_NAME, 'Validate')
  validate(validateRequestDto: ValidateRequestDto): Promise<ValidateResponse> {
    return this.authService.validate(validateRequestDto);
  }
}
