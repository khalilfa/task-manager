import { ValidateRequest } from "../auth.pb";
import { IsString } from 'class-validator';

export class ValidateRequestDto implements ValidateRequest {
  @IsString()
  public readonly token: string;
}