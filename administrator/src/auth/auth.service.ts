import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import * as bcrypt from 'bcrypt';
import { UserDto } from './dto/user.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt/jwt-payload.interface';
import { CreateUserResponse, SignInResponse, ValidateResponse } from './auth.pb';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ValidateRequestDto } from './dto/validateRequest.dto';

@Injectable()
export class AuthService {
  @InjectRepository(User)
  private readonly repository: Repository<User>;

  constructor(
    private jwtService: JwtService
  ) {}

  async createUser(userDto: UserDto): Promise<CreateUserResponse> {
    const { email, password, name } = userDto;

    // Hash the password
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    const user: User = this.repository.create({ email, name, password: hashedPassword });

    try {
      await this.repository.save(user);
    } catch (error) {
      if (error.code == 23505) {
        return { status: HttpStatus.CONFLICT, error: ['Email already exist in the database'] };
      } else {
        return { status: HttpStatus.INTERNAL_SERVER_ERROR, error: ['Error interno'] };
      }
    }

    return { status: HttpStatus.CREATED, error: null };
  }

  async signIn(authCredentialsDto: AuthCredentialsDto): Promise<SignInResponse> {
    const { email, password } = authCredentialsDto;

    const user = await this.repository.findOne({ where: { email } });

    if (user && (await bcrypt.compare(password, user.password))) {
      const payload: JwtPayload = { email };
      const accessToken: string = await this.jwtService.sign(payload);

      return { status: HttpStatus.OK, error: null, accessToken };
    } else {
      return {
        status: HttpStatus.NOT_FOUND,
        error: ['The user or the password is wrong'],
        accessToken: null
      };
    }
  }

  public async validate({ token }: ValidateRequestDto): Promise<ValidateResponse> {
    const decoded: JwtPayload = await this.jwtService.verify(token);

    if (!decoded) {
      return { status: HttpStatus.FORBIDDEN, error: ['Token is invalid'], userId: null };
    }

    const user: User = await this.repository.findOne({ where: { email: decoded.email } });

    if (!user) {
      return { status: HttpStatus.CONFLICT, error: ['User not found'], userId: null };
    }

    return { status: HttpStatus.OK, error: null, userId: user.id };
  }
}
