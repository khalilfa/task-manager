import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './jwt/jwt.strategy';
import { User } from './user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppModule } from 'src/app.module';

@Module({
  imports: [
    JwtModule.register({
      secret: 'facundopass',
      signOptions: { expiresIn: 3600 }
    }),
    TypeOrmModule.forFeature([User]),
    AppModule
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [TypeOrmModule]
})
export class AuthModule {}
