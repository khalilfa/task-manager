import { Controller } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './task.entity';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GetTasksQuery } from './queries/getTasks.query';
import { GetTasksByIdQuery } from './queries/getTaskById.query';
import { CreateTaskCommand } from './commands/createTask.command';
import { DeleteTaskCommand } from './commands/deleteTask.command';
import { UpdateTaskCommand } from './commands/updateTask.command';
import { GrpcMethod } from '@nestjs/microservices';
import { CreateTaskResponse, DeleteTaskResponse, GetTaskByIdResponse, GetTasksResponse, TASK_SERVICE_NAME, UpdateTaskResponse } from './task.pb';

@Controller()
export class TasksController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus
  ) {}

  @GrpcMethod(TASK_SERVICE_NAME, 'GetTasks')
  async getTasks({ userId }): Promise<GetTasksResponse> {
    const tasks: Task[] = await this.queryBus.execute(new GetTasksQuery(userId));

    return { tasks };
  }

  @GrpcMethod(TASK_SERVICE_NAME, 'GetTaskById')
  getTaskById({ userId, id }): Promise<GetTaskByIdResponse> {
    const task: Promise<Task> = this.queryBus.execute(new GetTasksByIdQuery(userId, id));
  
    return task;
  }

  @GrpcMethod(TASK_SERVICE_NAME, 'CreateTask')
  createTask(createTaskDto: CreateTaskDto): Promise<CreateTaskResponse> {
    console.log('ENTRA AL CREATE TASK DEL MICROSERVICIO')
    const task: Promise<Task> = this.commandBus.execute(new CreateTaskCommand(createTaskDto));

    return task;
  }

  @GrpcMethod(TASK_SERVICE_NAME, 'DeleteTask')
  deleteTask({ id, userId }): Promise<DeleteTaskResponse> {
    return this.commandBus.execute(new DeleteTaskCommand(userId, id));
  }

  // TODO: ERROR HANDLING
  @GrpcMethod(TASK_SERVICE_NAME, 'UpdateTask')
  updateTask(updateTaskDto: UpdateTaskDto): Promise<UpdateTaskResponse> {
    const task: Promise<Task> = this.commandBus.execute(new UpdateTaskCommand(updateTaskDto));

    return task;
  }
}