import { QueryHandler, IQueryHandler } from "@nestjs/cqrs";
import { TasksRepository } from "../task.repository";
import { Task } from "../task.entity";
import { NotFoundException } from '@nestjs/common';

export class GetTasksByIdQuery {
  constructor(public userId: string, public id: string) {}
}

@QueryHandler(GetTasksByIdQuery)
export class GetTasksByIdHandler implements IQueryHandler<GetTasksByIdQuery> {
  constructor(private readonly tasksRepository: TasksRepository) {}

  public async execute(query: GetTasksByIdQuery): Promise<Task> {
    const { id, userId } = query;
    const found = await this.tasksRepository.findOne({ where: { id, user: { id: userId } } });
    
    if (!found) {
      throw new NotFoundException(`Task with ID "${id}" not found`);
    }

    return found;
  }
}