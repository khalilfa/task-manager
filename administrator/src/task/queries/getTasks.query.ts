import { QueryHandler, IQueryHandler } from "@nestjs/cqrs";
import { TasksRepository } from "../task.repository";
import { Task } from "../task.entity";

export class GetTasksQuery {
  constructor(public userId: string) {}
}

@QueryHandler(GetTasksQuery)
export class GetTasksHandler implements IQueryHandler<GetTasksQuery> {
  constructor(private readonly tasksRepository: TasksRepository) {}

  public async execute(query: GetTasksQuery): Promise<Task[]> {
    return this.tasksRepository.getTasks(query.userId);
  }
}