import { GetTasksByIdHandler } from "./getTaskById.query";
import { GetTasksHandler } from "./getTasks.query";

export const QueryHandlers = [
  GetTasksByIdHandler,
  GetTasksHandler
];