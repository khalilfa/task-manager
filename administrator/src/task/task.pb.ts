/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "task";

/** Task */
export enum TaskStatus {
  TO_DO = 0,
  IN_PROGRESS = 1,
  DONE = 2,
  UNRECOGNIZED = -1,
}

export interface Task {
  id: string;
  title: string;
  description: string;
  status: TaskStatus;
}

/** GetTasks */
export interface GetTasksRequest {
  userId: string;
}

export interface GetTasksResponse {
  tasks: Task[];
}

/** GetTaskById */
export interface GetTaskByIdRequest {
  id: string;
  userId: string;
}

export interface GetTaskByIdResponse {
  id: string;
  title: string;
  description: string;
  status: TaskStatus;
}

/** CreateTask */
export interface CreateTaskRequest {
  title: string;
  description: string;
  userId: string;
}

export interface CreateTaskResponse {
  id: string;
  title: string;
  description: string;
  status: TaskStatus;
}

/** DeleteTask */
export interface DeleteTaskRequest {
  id: string;
  userId: string;
}

export interface DeleteTaskResponse {
  status: number;
  error: string[];
}

/** UpdateTask */
export interface UpdateTaskRequest {
  id: string;
  status: TaskStatus;
  title: string;
  description: string;
  userId: string;
}

export interface UpdateTaskResponse {
  id: string;
  title: string;
  description: string;
  status: TaskStatus;
}

export const TASK_PACKAGE_NAME = "task";

export interface TaskServiceClient {
  getTasks(request: GetTasksRequest): Observable<GetTasksResponse>;

  getTaskById(request: GetTaskByIdRequest): Observable<GetTaskByIdResponse>;

  createTask(request: CreateTaskRequest): Observable<CreateTaskResponse>;

  deleteTask(request: DeleteTaskRequest): Observable<DeleteTaskResponse>;

  updateTask(request: UpdateTaskRequest): Observable<UpdateTaskResponse>;
}

export interface TaskServiceController {
  getTasks(request: GetTasksRequest): Promise<GetTasksResponse> | Observable<GetTasksResponse> | GetTasksResponse;

  getTaskById(
    request: GetTaskByIdRequest,
  ): Promise<GetTaskByIdResponse> | Observable<GetTaskByIdResponse> | GetTaskByIdResponse;

  createTask(
    request: CreateTaskRequest,
  ): Promise<CreateTaskResponse> | Observable<CreateTaskResponse> | CreateTaskResponse;

  deleteTask(
    request: DeleteTaskRequest,
  ): Promise<DeleteTaskResponse> | Observable<DeleteTaskResponse> | DeleteTaskResponse;

  updateTask(
    request: UpdateTaskRequest,
  ): Promise<UpdateTaskResponse> | Observable<UpdateTaskResponse> | UpdateTaskResponse;
}

export function TaskServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["getTasks", "getTaskById", "createTask", "deleteTask", "updateTask"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("TaskService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("TaskService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const TASK_SERVICE_NAME = "TaskService";
