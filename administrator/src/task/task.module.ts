import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksController } from './task.controller';
import { TasksRepository } from './task.repository';
import { AuthModule } from 'src/auth/auth.module';
import { QueryHandlers } from './queries';
import { CommandHandlers } from './commands';
import { CqrsModule } from '@nestjs/cqrs';
import { AppModule } from 'src/app.module';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([TasksRepository]),
    AuthModule,
    AppModule
  ],
  controllers: [TasksController],
  providers: [
    TasksRepository,
    ...QueryHandlers,
    ...CommandHandlers
  ],
})

export class TasksModule {}