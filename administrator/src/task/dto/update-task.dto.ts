import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { TaskStatus } from '../task-status.enum';

export class UpdateTaskDto {
  @IsEnum(TaskStatus)
  @IsOptional()
  status: TaskStatus;

  @IsOptional()
  title: string;

  @IsOptional()
  description: string;

  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  userId: string;
}