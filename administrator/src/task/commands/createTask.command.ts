import { CreateTaskDto } from "../dto/create-task.dto";
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { TasksRepository } from "../task.repository";
import { Task } from "../task.entity";
import { Repository } from "typeorm";
import { User } from "src/auth/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Injectable } from "@nestjs/common";

export class CreateTaskCommand {
  constructor(public createTaskDto: CreateTaskDto) {}
}

@Injectable()
@CommandHandler(CreateTaskCommand)
export class CreateTaskHandler implements ICommandHandler<CreateTaskCommand> {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly tasksRepository: TasksRepository
  ) {}

  public async execute(command: CreateTaskCommand): Promise<Task> {
    const user = await this.userRepository.findOne({ where: { id: command.createTaskDto.userId } });
    return this.tasksRepository.createTask(command.createTaskDto, user);
  }
}