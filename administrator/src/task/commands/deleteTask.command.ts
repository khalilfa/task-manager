import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { TasksRepository } from "../task.repository";
import { HttpStatus, NotFoundException } from '@nestjs/common';
import { DeleteTaskResponse } from "../task.pb";

export class DeleteTaskCommand {
  constructor(public userId: string, public id: string) {}
}

@CommandHandler(DeleteTaskCommand)
export class DeleteTaskHandler implements ICommandHandler {
  constructor(private readonly tasksRepository: TasksRepository) {}

  public async execute(command: DeleteTaskCommand): Promise<DeleteTaskResponse> {
    const { id, userId } = command;

    const result = await this.tasksRepository.delete({ id, user: { id: userId } });

    if (result.affected === 0) {
      return { status: HttpStatus.NOT_FOUND, error: [`Task with ID "${id}" not found`] };
    }

    return { status: HttpStatus.OK, error: [] };
  }
}