import { UpdateTaskDto } from "../dto/update-task.dto";
import { CommandHandler, ICommandHandler } from "@nestjs/cqrs";
import { TasksRepository } from "../task.repository";
import { Task } from "../task.entity";

export class UpdateTaskCommand {
  constructor(public updateTaskDto: UpdateTaskDto) {}
}

@CommandHandler(UpdateTaskCommand)
export class UpdateTaskHandler implements ICommandHandler {
  constructor(private readonly tasksRepository: TasksRepository) {}

  public async execute(command: UpdateTaskCommand): Promise<Task> {
    const { updateTaskDto } = command;
    const { status, title, description, id, userId } = updateTaskDto;

    const task: Task = await this.tasksRepository.getTaskById(id, userId);

    status && (task.status = status);
    title && (task.title = title);
    description && (task.description = description);

    await this.tasksRepository.save(task);

    return task;
  }
}