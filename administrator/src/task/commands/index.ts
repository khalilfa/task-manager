import { CreateTaskHandler } from "./createTask.command";
import { DeleteTaskHandler } from "./deleteTask.command";
import { UpdateTaskHandler } from "./updateTask.command";

export const CommandHandlers = [
  CreateTaskHandler,
  DeleteTaskHandler,
  UpdateTaskHandler
];