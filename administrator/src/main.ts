import { INestMicroservice, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { protobufPackage as authProto } from './auth/auth.pb';
import { protobufPackage as taskProto } from './task/task.pb';
import { join } from 'path';
import { HttpExceptionFilter } from './auth/filter/http-exception.filter';
import { AuthModule } from './auth/auth.module';
import { TasksModule } from './task/task.module';

async function bootstrap() {
  const authApp: INestMicroservice = await NestFactory.createMicroservice(AuthModule, {
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:3002',
      package: authProto,
      protoPath: join('./src/proto/auth.proto'),
    },
  });

  authApp.useGlobalFilters(new HttpExceptionFilter());
  authApp.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  const taskApp: INestMicroservice = await NestFactory.createMicroservice(TasksModule, {
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:3003',
      package: taskProto,
      protoPath: join('./src/proto/task.proto'),
    },
  });

  taskApp.useGlobalFilters(new HttpExceptionFilter());
  taskApp.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  await Promise.all([authApp.listen(), taskApp.listen()]);
}

bootstrap();
