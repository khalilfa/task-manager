import { Body, Controller, Inject, OnModuleInit, Post } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import {
  AuthServiceClient,
  CreateUserRequest,
  CreateUserResponse,
  AUTH_SERVICE_NAME,
  SignInRequest,
  SignInResponse
} from './auth.pb';

@Controller('auth')
export class AuthController implements OnModuleInit {
  private svc: AuthServiceClient;

  @Inject(AUTH_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<AuthServiceClient>(AUTH_SERVICE_NAME);
  }

  @Post('create-user')
  private async createUser(@Body() body: CreateUserRequest): Promise<Observable<CreateUserResponse>> {
    return this.svc.createUser(body);
  }

  @Post('signin')
  private async signIn(@Body() body: SignInRequest): Promise<Observable<SignInResponse>> {
    return this.svc.signIn(body);
  }
}