import {
  Controller,
  Inject,
  Post,
  OnModuleInit,
  UseGuards,
  Req,
  Get,
  Param,
  Delete,
  Put,
  Body
} from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';
import {
  TaskServiceClient,
  TASK_SERVICE_NAME,
  GetTasksResponse,
  GetTaskByIdResponse,
  CreateTaskResponse,
  DeleteTaskResponse,
  UpdateTaskResponse,
  CreateTaskRequest,
} from './task.pb';
import { AuthGuard, CustomRequest } from '../auth/auth.guard';

@Controller('task')
@UseGuards(AuthGuard)
export class TaskController implements OnModuleInit {
  private svc: TaskServiceClient;

  @Inject(TASK_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<TaskServiceClient>(TASK_SERVICE_NAME);
  }

  @Get()
  private async getTasks(@Req() req: CustomRequest): Promise<Observable<GetTasksResponse>> {
    req.body.userId = req.user;

    return this.svc.getTasks(req.body);
  }

  @Get('/:id')
  private async getTaskById(@Param('id') id: string, @Req() req: CustomRequest): Promise<Observable<GetTaskByIdResponse>> {
    req.body.userId = req.user;
    req.body.id = id;

    return this.svc.getTaskById(req.body);
  }

  @Post()
  private async createTask(@Body() createTaskRequest: CreateTaskRequest): Promise<Observable<CreateTaskResponse>> {
    console.log('ENTRA AL CREATETASK');
    return this.svc.createTask(createTaskRequest);
  }

  @Delete('/:id')
  private async deleteTask(@Param('id') id: string, @Req() req: CustomRequest): Promise<Observable<DeleteTaskResponse>> {
    req.body.userId = req.user;
    req.body.id = id;

    return this.svc.deleteTask(req.body);
  }

  @Put('/:id')
  private async updateTask(@Param('id') id: string, @Req() req: CustomRequest): Promise<Observable<UpdateTaskResponse>> {
    req.body.userId = req.user;
    req.body.id = id;

    return this.svc.updateTask(req.body);
  }
}