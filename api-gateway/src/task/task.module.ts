import { Module, Global } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TASK_SERVICE_NAME, TASK_PACKAGE_NAME } from './task.pb';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';

@Global()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: TASK_SERVICE_NAME,
        transport: Transport.GRPC,
        options: {
          url: '0.0.0.0:3002',
          package: TASK_PACKAGE_NAME,
          protoPath: './src/proto/task.proto',
        },
      },
    ]),
  ],
  controllers: [TaskController],
  providers: [TaskService],
  exports: [TaskService],
})
export class TaskModule {}