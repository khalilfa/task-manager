import { Inject, Injectable } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { TaskServiceClient, TASK_SERVICE_NAME } from './task.pb';

@Injectable()
export class TaskService {
  private svc: TaskServiceClient;

  @Inject(TASK_SERVICE_NAME)
  private readonly client: ClientGrpc;

  public onModuleInit(): void {
    this.svc = this.client.getService<TaskServiceClient>(TASK_SERVICE_NAME);
  }
}