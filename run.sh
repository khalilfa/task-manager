#!/bin/bash

# Construir las imágenes de Docker
docker build -t api-gateway ./api-gateway
docker build -t administrator ./administrator

# Iniciar la aplicación con Docker Compose
docker-compose up